## Python Club ending

The Python Club will be ending as an experiment for both Beginners and Intermediate/Advance groups due to the declining attendance. 

I want to recognize and express my gratitude to those of you that provided help and support throughout the duration of the club, and all of you for your early enthusiasm. Hopefully you will have gained some insight and skills, and discovered something new. That is certainly true for me.

Session summaries will remain extant within the [**pythonclub GitLab**](https://git.doit.wisc.edu/bcrf/pythonclub) repositories for future references about the studied materials or links to other resources. The [**LinkedIn -Learning**](http://linkedinlearning.com) courses are a professional source of learning materials available to you at UW.

Here are useful resources from the on-Campus Data Science Hub:

>If you need specific help in coding (Python or other languages) the Data Science Hub’s [**Coding Meetup**](https://datascience.wisc.edu/hub/#dropin) offers one-on-one help (Tuesdays and Thursdays, 2:30–4:30 p.m..) 

>I had hoped to invite outside speakers on specific topics, particularly machine learning (ML.) If that is your interest then the Data Science Hub ["**ML Community**"](https://datascience.wisc.edu/ml-community/) events and coffee socials may of interest.


I will resume a more diverse set of instruction materials useful for Biochemists that will range from command-line in `bash` to creating Augmented Reality molecular models (in preparation,) but also materials from the Software Carpentry. 

A new Python Club, or Python Circle may still manifest in the future, in a different form, perhaps as an IPib or Student group that may have funds for offering food items...?!

If you have any questions or concerns, please do not hesitate to reach out to me. I have enjoyed the process and the learning, and mostly getting to know you.

My door and email are always open.

Sincerely,

Jean-Yves

P.S. This doe NOT affect the R Club for now, pending attendance.
