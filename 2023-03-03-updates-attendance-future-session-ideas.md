[TOC]

# Python Club updates

Below are updates on:

- highlights of the last session for each StudyGroup
- attendance and the future of the Clubs
- future session ideas for Intermediate/Advanced group

Pages and Repositories reminders:

- Club page: https://bcrf.biochem.wisc.edu/python-club/

- GitLab: https://git.doit.wisc.edu/bcrf/pythonclub

	- [Communication](https://git.doit.wisc.edu/bcrf/pythonclub/communication) 
	- [Beginners](https://git.doit.wisc.edu/bcrf/pythonclub/beginners)
	- [Intermediate/Advanced](https://git.doit.wisc.edu/bcrf/pythonclub/intermediate_advanced)

## Beginners session 08

[**2023-03-02-session-08**](https://git.doit.wisc.edu/bcrf/pythonclub/beginners/studygroup/-/tree/main/2023-Spring/2023-03-02-session-08) - Attendance: 2

We reviewed what is presented in video for section 3 **Basic Data Types*** in  
[***Python Essential Training***](https://www.linkedin.com/learning/python-essential-training-18764650/getting-started-with-python) with the added video for list in section 4 (total video time is about 30min.)  Relevant [Jupyter notebooks](https://github.com/LinkedInLearning/python-essential-training-4314028):

```
03_01_Ints_and_Floats.ipynb
03_02_Other_Numbers.ipynb
03_03_Booleans.ipynb
03_04_Strings.ipynb
03_05_bytes.ipynb
03_06_Challenge.ipynb
03_06_Challenge_Hints.ipynb
03_07_Solution.ipynb
04_01_lists.ipynb
```

### Adding time (minutes:seconds) with help of ChatGPT

We explored how to use a Python `list` to add time in the form time stamps in `minutes:seconds`. Files are within the session folder on GitLab for [Session-08](https://git.doit.wisc.edu/bcrf/pythonclub/beginners/studygroup/-/tree/main/2023-Spring/2023-03-02-session-08)

## Intermediate/Advanved session 04

[2023-02-28-session-04](https://git.doit.wisc.edu/bcrf/pythonclub/intermediate_advanced/studygroup/-/tree/main/2023-Spring/2023-02-28-session-04) - Attendance: 2

Session theme: **Graphical User Interface with Python**

### Highlights:

- We looked at the examples in [***Create Graphical User Interfaces with Python***](https://www.dbooks.org/create-graphical-user-interfaces-with-python-1912047918/). ([Jupyter Notebooks](https://github.com/themagpimag/createguis).)
- Explored how ChatGPT could help convert a simple protein sequence composition script (developped in the Beginners section) into a GUI version.

Python packages used `guizero` and `tk` (tkinter)


# The future of Python Club(s)

The Club is not a class, and schedules can be difficult, which is one reason attendance has not been consistent. 

The Python and R Clubs are an experiment. It may be that the Club modalities are not what is useful. It could be the formats, the books, the change to "flip" format for Beginners...

If attendance does not pick-up, these Clubs will be closed and replaced with *something else*... I have an idea to be shared later.

### Table of attendance since January

> Beginners group meets every week for about 1h.

|  Beginners | attendance  |
|---|---|
| Jan 12  | 17  |
|  Jan 19 |  4 |
| Jan 26  |  7 |
|  Feb 2 |  5 |
|  Feb 9 | 4 |
|  Feb 16 | 5 |
|  Feb 23 | 2 |
|  Mar 2 | 2 |

> Intermediate/Advance group meets every other week for about 2hours.

|  Interm/Adv |  attendance |
|---|---|
| Jan 17  | 7  |
| Jan 31  | 5  |
| Feb 14  | 6 |
| Feb 28  | 2 |


## Intermediate/Advanced ideas for future sessions

In a [previous message](https://git.doit.wisc.edu/bcrf/pythonclub/communication/-/blob/main/2023-02-23-survey-results-comments.md) reporting the survey answers I suggested that the Intermediate/Advanced group also switch to a Flip format:

>The **Intermediate/Advanced group** could decide, as a group, to also switch to a "flip" format by following a more advanced LinkedIn Course. Here are some options:

>- [Python Data Analysis](https://www.linkedin.com/learning/python-data-analysis-2) by Michele Vallisneri, Theoretical Astrophysicist at NASA Jet Propulsion Laboratory. 
>- [Intermediate Python for Non-Programmers](https://www.linkedin.com/learning/intermediate-python-for-non-programmers), Nick Walter,  Mobile and web developer, instructor, and coding enthusiast
>- Searching for the word "Python" will yield more results.

There was no reaction to that suggestion, so below are other possible topics to explore:


- Wikipedia
- Web scrapping
- De novo design of luciferases using deep learning
- Dashboads



### Wikipedia

[wikipedia 1.4.0](https://pypi.org/project/wikipedia/): Access wikipedia from within Python. 

 Easily access and parse data from Wikipedia: search , get article summaries, get data (*e.g* links and images.) Wraps the [MediaWiki API](https://www.mediawiki.org/wiki/API) so you can focus on using Wikipedia data.
 
* `pip install wikipedia`
* [Quickstart](https://wikipedia.readthedocs.io/en/latest/quickstart.html)

---

### Web scrapping

*Web scraping* has also been caleld *screen scraping*, *data mining*, or *web harvesting*

* *Book*: **Web Scraping with Python : Collecting More Data from the Modern Web**,  2018, Ryan Mitchell. 
(Campus availability: Physical - Location: Memorial Library, Stacks Regular Size Shelving. Catalog Number:
[QA76.73.P98 M58 2018](https://search.library.wisc.edu/catalog/9912653520702121) - `CODE`: 
	- 2018: https://github.com/REMitchell/python-scraping
	- 2014: https://www.pythonscraping.com/code/

* *Book*: **Website scraping with Python : using BeautifulSoup and Scrapy** UW-Library [Catalog](https://search.library.wisc.edu/catalog/9912636871802121) => Via UW-Library: [ONLINE by SpringerLink eBooks](https://link-springer-com.ezproxy.library.wisc.edu/book/10.1007/978-1-4842-3925-4) Free download PDF, Epub. - General [Not UW link](https://link.springer.com/book/10.1007/978-1-4842-3925-4) can also download PDF. `Source Code`: 
	- https://github.com/Apress/website-scraping-w-python

There are many possible implementations for this:

* `urllib` is a package built for web scraping from [Python Standard Library](https://docs.python.org/3/library/)
Example: https://realpython.com/python-web-scraping-practical-introduction/

* `scrapy` is another package built for web scraping.

* Yet another solution is with packages `requests` and `BeautifulSoup`:   
	- https://oxylabs.io/blog/python-web-scraping   
	- https://www.geeksforgeeks.org/python-web-scraping-tutorial/
	- http://faculty.washington.edu/otoomet/machinelearning-py/web-scraping.html

* Web Scraping Using Selenium Python: https://iqss.github.io/dss-webscrape/
	- Selenium is an open-source tool that automates web browsers.  

---

### De novo design of luciferases using deep learning

Nature | Vol 614 | 23 February 2023 | 775

https://doi.org/10.1038/s41586-023-05696-3

>*de novo* protein design to create luciferases [...] specific for one substrate and need no cofactors to function. We chose a synthetic luciferin, diphenylterazine (DTZ) [...] To identify protein folds that are capable of hosting such pockets, we first docked DTZ into 4,000 native small-molecule-binding proteins. [...] we developed a deep-learning-based ‘family-wide hallucination’ approach that integrates unconstrained de novo design17,18 and Rosetta sequence-design approaches [...].  
>[...] All relevant scripts and an accompanying Jupiter notebook for family-wide hallucination scaffold generation are available

***Data and scripts are available. Could we reproduce this research?***


**Data availability**

Source data for Figs. 2–4 are available online. The gene sequence for LuxSit-i has been deposited to GenBank under the accession number OP820699. See [Supplementary Data](https://www.nature.com/articles/s41586-023-05696-3#MOESM4) for design models of LuxSit and LuxSit-i. Codon-optimized plasmids encoding LuxSit-i for bacterial and mammalian expression are available through Addgene. [Source data](https://www.nature.com/articles/s41586-023-05696-3#Sec13) are provided with this paper.


**Code availability**

The Rosetta macromolecular modelling suite (https://www.rosettacommons.org) is freely available to academic and non-commercial users. Commercial licences for the suite are available through the University of Washington Technology Transfer Office. The source code for RIF docking implementation is freely available at https://github.com/rifdock/rifdock. All relevant scripts and an accompanying Jupiter notebook for family-wide hallucination scaffold generation are available here: https://files.ipd.uw.edu/pub/luxSit/scaffold_generation.tar.gz. All generated scaffolds are available here: https://files.ipd.uw.edu/pub/luxSit/scaffolds.tar.gz. Computational design scripts for the luciferase libraries are available here: https://files.ipd.uw.edu/pub/luxSit/luciferase_designs_methods.zip.



---

### Dashboads

#### Streamlit

**Streamlit** is a package to build dashboards. Content can be interactive if using **plotly**. Example created by ChatGPT:

- **`code`**:  https://github.com/gabrielzandonadi/Iris-GPT3-Analysis
- **dataset (Iris)**: https://www.kaggle.com/datasets/uciml/iris
- **inspiration**: https://youtu.be/wzpl_txHtUQ
- **Dashboard**: https://gabrielzandonadi-iris-gpt3-analysis-iris-ag8o65.streamlit.app
- Dashboards from the YouTube video inspiration: 
	- Iris dataset: https://holidaycodingsessions.streamlit.app
	- Breast Cancer Classifier: https://holidaycodingsessions.streamlit.app/ml_app

From LinkedIn Post https://www.linkedin.com/feed/update/urn:li:activity:7023597024168026112 - login required. Text below:

> Just finished building this awesome EDA dashboard for the #Iris dataset using #Streamlit, #Pandas, #Seaborn and #Plotly! It allows for easy exploration of the data using various types of plots, summary statistics, and correlation. Check it out!

> Exploratory Dashboard made by GPT 3 : https://gabrielzandonadi-iris-gpt3-analysis-iris-ag8o65.streamlit.app

> But what really made this project stand out was the use of #GPT3 for the exploratory analysis. It helped me to quickly generate ideas for visualizations and insights that I would have otherwise missed. Using GPT-3 for data analysis is like having a virtual data scientist by your side, providing you with suggestions and insights in real-time.

>The Iris dataset is a collection of information about different types of flowers. Each flower is described by 4 different measurements: the length and width of the petals and the length and width of the sepals. These measurements are used to identify 3 different species of Iris flowers: setosa, versicolor, and virginica.

>Feel free to ask anything ✍ 

#### PyGWalker

Another nice option is: [PyGWalker](https://github.com/Kanaries/pygwalker) can simplify your Jupyter Notebook data analysis and data visualization workflow, by turning your pandas dataframe into a Tableau-style User Interface for visual exploration.

---

