(Reminder: raw results are available in [BOX](https://uwmadison.box.com/s/55stek5bgn265ygxym38fra4p5vwsyrq))


## Python Club - Survey results and comments

Thank you again for the 11 answers, 7 from beginners and 4 from intermediate/advanced, and particularly to the longer text answers.

I will concentrate on the remarks of free-text answers from question 6: 
>Q6 - - To what extent are your expectations for the club being met?  - Is there anything you would like to see done differently in the Python Club?  - If so, what changes would you suggest?

### Highlights

Here are the key elements that I noted:

- more exercises, importance of ongoing practice 
- more basic principles
- build a foundation and learn tips (tricks)
- ask (Python-related) questions
- ainformal learning: review / discuss / solve exercises

On the down side was:

- lack or little motivation
- few people will volunteer 
- class style not helpful

Alternate format suggestions:

- Office hours
- watch videos from short course and discuss in club


In a "word cloud" plot of the answers the follow words were more promient:

- meet
- practice
- time
- learn
- example
- problem
- exercise
- language

### Change of format

I think that most items on this list will be addressed by the suggested change that the **Beginners Study Group** will experience in the first "***Flip Class Session***" based on a LinkedIn Learning course [***Python Essential Training***](https://www.linkedin.com/learning/python-essential-training-18764650/getting-started-with-python). All of these professionally created lessons are accompanied by exercises. For this specific course they are also available as ***Jupyter notebooks*** on [github](https://github.com/LinkedInLearning/python-essential-training-4314028).

This will take care of:

- more exercises
- foundation and basic principles
- worked examples
-  "watch videos" suggested format change

The suggestion of "Office hours" is one that I had pondered, but I am personally not an expert Python programmer. However, there is a Campus-wide  service for this purpose from the Data Science Hub: 

* [**Coding Meetup**](https://datascience.wisc.edu/hub/#dropin) Tuesdays and Thursdays, 2:30–4:30 p.m. CT. (See link for details.)


## How to learn?

Everyone is different, and methods to learn vary. When I was in 1^th grade in France we first learned the alphabet ***before*** learning words. But then came the "modern and new" *"global method"* that was teaching complete words (and sounds) without learning the alphabet first. The method probably came from the USA 'Phonics" method but I can't be sure.

The point is that as a Beginner it is difficult to "dive" into a Python script that is more than a few lines of code and hope to figure it all out while not knowing the foundational basics. This "global" method would require a trained teacher and might not work for everyone.

This is why, in the early sessions, currently, we have to go over the basics of *Python notation*, *basic data types*, *operators*, *flow control*... It's a bit cumbersome, but without this foundation one is quickly lost.

### Reflecting questions...

Ultimately the question is: why do I need to learn Python?

Do I need Python for any specific type of analysis for the Lab, or for my Master project or PhD?

Is Python the best software for this? Or could I just do what I need with Excel? What about R?

Is the data I have or will have a good match to be analyzed in Python?

DO I need to create new code, or can I find existing code using Google and modify that code to match with my data and suit my needs? (This is what is most useful for me.)

Nowadays it may even be possible to as AI help in creating code. It often is easier to follow and understand pre-written code than to summon it. It is also possible to phrase the request so that the AI will add comments within the script to explain the code. ***Perhaps AI sessions could be fun to try out?!***

Do I have the time to spare for the Club? Regularly?

If I watch the videos on my own, what's the use to going to the club? (possibly to ask questions and share...)

All of these questions are relevant to maintain a certain level of motivation, and find an interest to keep exploring the possibilities, and keep the enthusiasm to learn.

And this is very personal.

## What next?

The **Intermediate/Advanced group** could decide, as a group, to also switch to a "flip" format by following a more advanced LinkedIn Course. Here are some options:

- [Python Data Analysis](https://www.linkedin.com/learning/python-data-analysis-2) by Michele Vallisneri, Theoretical Astrophysicist at NASA Jet Propulsion Laboratory. 
- [Intermediate Python for Non-Programmers](https://www.linkedin.com/learning/intermediate-python-for-non-programmers), Nick Walter,  Mobile and web developer, instructor, and coding enthusiast
- Searching for the word "Python" will yield more results.

But, as stated in a previous message, for the next session (February 28) we'll have fun exploring how to create a GUI interface for Python programs with [***Create Graphical User Interfaces with Python***](https://www.dbooks.org/create-graphical-user-interfaces-with-python-1912047918/).

A Club is also a way to connect to others, and find common ground.
If we need a different Club format, suggestions are always welcome.

Hopefully a number of current members will find it useful to participate in the Club, share and learn more Python skills. 

At least it is my sincere hope.

My door (and email) is always open to chat, or answer questions...

Best wishes to all,

Jean-Yves










