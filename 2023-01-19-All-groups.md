
# Python Club Spring 2023 - January

Here is information about the coming sessions for both groups for the rest of January. 
Additional details can be found in your respective GitLab repositories (links below.)

## Beginners - January 19 & 26, 2023

The doors of room 201 will be opened by one of the group (Haley Deorio volunteered) so that you can meet on those 2 January dates to keep the Club momentum going while I am out of town. 

The classroom will be opened by  on Thursdays 19 and 26 of January.

I have prepared Jupyer Notebooks from Chapters 2 and 3 of the book that we follow, for these 2 session. This will be an opportunity to become familiar with this interface. Jupyter notebooks are often used to share code online. It is easy to annotate the code and provide details about the data, the results etc. 

The material is in the GitLab repository:

https://git.doit.wisc.edu/bcrf/pythonclub/beginners/studygroup

### Thursday Jan 19

*Please check-in the sign-up sheet*.

* Click on folder `2023-Spring`
* Then `2023-01-19-Beginners-session-02`
	* Scroll down to read the information contained within the `Readme.md` file and follow instructions

### Thursday Jan 26

*Please check-in the sign-up sheet*.

* Use notebook in directory `2023-01-26-Beginners-session-03`.

### Suggestion:

Learn a minimum of **Mardown** text, it's easy and simple and is what can make your Jupyter Notebook annotations clearer and more presentable. See this resource: [Do yourself a favor: learn Markdown – Master it in 10 minutes!](https://bcrf.biochem.wisc.edu/2021/06/03/do-yourself-a-favor-learn-markdown-master-it-in-10-minutes/)

## Intermediate/Advanced - January 31

Gitlab repository for this group is: https://git.doit.wisc.edu/bcrf/pythonclub/intermediate_advanced/studygroup

I added a summary for January 17th short session (link below.)

Since this is a more advanced group, we'll jump forward to Chapter 6, and at the next session (January 31^st ) Cristian will present questions related to Chapter 6 and Regular Expressions.

Please refer to the **session summary** [`2023-01-17-session-01.md`](https://git.doit.wisc.edu/bcrf/pythonclub/intermediate_advanced/studygroup/-/blob/main/2023-Spring/2023-01-17-session-01.md) file in the GitLab repository for links about Regular Expression.

>### Trivia note: regular expression invented at UW-Madison

> [Regular expressions](https://en.wikipedia.org/wiki/Regular_expression) were invented in 1951 by Math professor [Stephen Cole Kleene](https://en.wikipedia.org/wiki/Stephen_Cole_Kleene) to describe a neural networks. (Links are Wikipedia.)
 
Jean-Yves

(PS. Yes, this email was originally written in markdown!)



