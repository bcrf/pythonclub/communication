
# Python Club 17 Jan 2023

For now I still need to send to all registered. I hope to have group-specific email lists in the near future.

## Intermediate/Advanced 

Today's planned session will somewhat interfere with the iPiB poster session.

However, we can still meet and get things organized for future events.

A Gitlab repository for this group is:

https://git.doit.wisc.edu/bcrf/pythonclub/intermediate_advanced/studygroup

## Beginners

For Thursday (Jan 19) session refer to the GitLab repository:

https://git.doit.wisc.edu/bcrf/pythonclub/beginners/studygroup

* Click on `2023-Spring`
* Then `2023-01-19-Beginners-session-02`
	* Scroll down to read the information contained within the `Readme.md` file and follow instructions;

The classroom will be opened by Haley Deorio (Keck Lab) on Thursdays 19 and 26 of January.




