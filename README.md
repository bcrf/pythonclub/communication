# ComBEE Study group

The Python Club has closed.

However, interested users can join the Madison group interested in *"Computational Biology, Ecology, & Evolution"* (ComBEE):

> [**ComBEE**](https://combee-uw-madison.github.io/studyGroup/) is a group of researchers at UW-Madison interested in computational biology in ecology and evolution.  
> (See Calendar within for sessions dates and time.)

# 2023 - Communication

This repository is meant to contain most of the communication to the Beginners and/or the Intermediate/Advanced group that is usually sent by email to the club members. This is a way to find previously sent messages more easily. 

Some information is also available on the BCRF Python Club page: https://bcrf.biochem.wisc.edu/python-club/

Organizer: Jean-Yves Sgro, jsgro@wisc.edu

