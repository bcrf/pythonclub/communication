## Python Club - New directions

*February 16, 2023*

Salutations to all,

The Beginners group met today at 4pm and we discussed the questions from my previous email about how the Python Club is doing. In order for everyone to get a chance to be heard, I prepared a questionnaire with just 10 short questions. I worded them so that most can be answered by multiple-choice -clicking-button, but there is also space for you to write your own remarks. None are mandatory.

### Quick survey

This is *anonymous* and voluntary and is meant for ***both*** *Beginners* and *Intermediate/Advanced* groups:

<center>
### **https://go.wisc.edu/8k8jw8** [^1]
</center>

[^1]: https://uwmadison.co1.qualtrics.com/jfe/form/SV_doi8ui68nTNAr8a


I will share the results next week to give enough time to answer.

## Beginners Sessions

In *Session 06 today* we briefly reviewed "Dictionary" and the code from last week. (in [ChatGPT\_Protein\_Composition.ipynb](https://git.doit.wisc.edu/bcrf/pythonclub/beginners/studygroup/-/blob/main/2023-Spring/2023-02-09-Beginners-session-05/ChatGPT_Protein_Composition.ipynb) from *session-05*.) We spent time considering how to verify the validity of the results using `bash` scripts.

We then looked at a real life example when I needed the one-letter-aa-code of proteins within Protein Data Bank (PDB) files in **FastA** sequence format. This is an example of finding code online and implementing it quickly. See code  in [PDB-to_fasta.ipynb](https://git.doit.wisc.edu/bcrf/pythonclub/beginners/studygroup/-/tree/main/2023-Spring/2023-02-16-session-06).

### Proposed new format

This is a question in the survey above, but in tonight's discussion during the Beginners session  there was a strong interest to change course and adopt a "Flip" format, which would mean:

- following a *LinkedIn Learning* Python course (free to UW) in small increments.
- About 20 to 30 min of video watched before sessions
- During session we will recap the video and also review the exercise files
- Exercise files  are in Jupyter Notebook, available on [github](https://github.com/LinkedInLearning/python-essential-training-4314028).

A quick search seems to indicate that the first course will be about 4hours long:

[***Python Essential Training***](https://www.linkedin.com/learning/python-essential-training-18764650/getting-started-with-python) - ***Getting started with Python***

This is a first of a series that we could follow, that has a total of 15 hours.

### Beginners session 07 - February 23, 2023

We'll start the new format next week, February 23, 2023.

Finding the course:

1. Courses are free to all UW personnel (thus NetID is necessary,) and can be accessed *via* [`my.wisc.edu`](https://my.wisc.edu/) with the square button to LinkedIn Learning.
2. However, the link: [***Python Essential Training***](https://www.linkedin.com/learning/python-essential-training-18764650/getting-started-with-python) may get you there directly (will still need NetID.)

For next week watch the following sections:

* . **Introduction**
* 1. **Gearing up for Python**
* 2. **QuickStart**

This totals **55 min 5sec**. of introductory material. For subsequent session we'll plan shorter video segments.


## Intermediate/Advanced

I am not sure if the "Flip" format would benefit this group in the same way, but this would be a good subject of reflection for the next session (February 28).

Concerning the subject of the last session, I suggested to read Chapter 15 of the book mentioned in the [previous email](https://git.doit.wisc.edu/bcrf/pythonclub/communication/-/blob/main/2023-02-16-new-material.md). 

I also added an etnry on the web site today: [Python Concepts of Object-Oriented Programming](https://bcrf.biochem.wisc.edu/2023/02/16/python-concepts-of-object-oriented-programming/) which was a fun opportunity for me to play with ChatGPT in preparing that page.

Any volunteers for next session?



