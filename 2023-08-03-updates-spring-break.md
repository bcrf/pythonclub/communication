## Python Club updates - 03-08-2023

Note: next week is **Spring Break** but the **Python sessions will be open**. We can perhaps take this opportunity to review some material, discuss more options (see more below,) and evaluate how the Club is going.

## Beginners - 03-09-2023

In the Flip format using 
[*Python Essential Training*](https://www.linkedin.com/learning/python-essential-training-18764650/getting-started-with-python), ([code](https://github.com/LinkedInLearning/python-essential-training-4314028))
the next section to watch is section *"4.  Basic Data Structures"* which adds to 32min 27 of video.

We'll go over the details during the session. If you cannot attend, bring your questions to the subsequent session.

## Intermediate/Advanced

Based on a previous email communication ([2023-03-03-updates...](https://git.doit.wisc.edu/bcrf/pythonclub/communication/-/blob/main/2023-03-03-updates-attendance-future-session-ideas.md)) I received 3 emails.

- **2** voted for studying the code and methods in the paper ["De novo design of luciferases using deep learning"](https://doi.org/10.1038/s41586-023-05696-3). It may (or may not?) be possible to follow all of it. But it certainly is a very interesting and at the forefront of the field.
- **1** voted for:
	- suggested Flip class for [Python Data Analysis](https://www.linkedin.com/learning/python-data-analysis-2) 
	- Web scrapping
	- A suggested book: *"Impractical Python Projects: Playful programming Activities to make you Smarter"* introducing advanced concepts. (PDF findable with a web search.)

If you want to vote send me an email.

I know that many will be taking advantage of Spring Break, a time to enjoy.

I hope to see some of you again in the next few weeks.

JYS
