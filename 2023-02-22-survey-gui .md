## Python Club - 

*February 22, 2023*

Greetings all **43** Club members,

Last week I sent a very short, *anonymous* survey that obtained a measly **3** answers. Perhaps we can do better than this... There is still time... The survey goal was:

>These questions aim to gather feedback from the Python Club members about the current book and the club in general, and preferences for potential changes. This questionnaire is voluntary and anonymous.

There are 10 short questions. I worded them so that most can be answered by multiple-choice but there is also space for you to write your own remarks. None are mandatory.
 If you'd like to express your opinion, go to this link: **https://go.wisc.edu/8k8jw8** [^1]

*Now for the fun stuff*:

## Intermediate/Advanced

During the last session I mentioned the possibility of exploring how to construct GUI apps using Python and that seemed to evoke a strong interest. For the session on February 23 we could explore this book, **free to download**: [***Create Graphical User Interfaces with Python***](https://www.dbooks.org/create-graphical-user-interfaces-with-python-1912047918/)

The approach is simple but in a style that makes it easy to learn. They also have a chapter of what *not* to do concerning GUI. I recently learned about **GUE** = Graphical User **Experience**. And I can say that I have found many "bad" GUI, on web site and various software.

Since there were no volunteers for the next session I propose that we explore the GUI world...


## Beginners

We'll explore the "Flip class" format at the next session.

### Don't forget the survey!





[^1]: https://uwmadison.co1.qualtrics.com/jfe/form/SV_doi8ui68nTNAr8a



