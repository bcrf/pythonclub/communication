# Python Club Spring 2023 - January/February

Updates on this week sessions.

## Intermediate/Advanced 


### Summary - January 31 - Session 02

The group met at 4pm and Cristian took us enthusiastically through an example of the use of regular expression to find long `GT` repeats in the human chromosome. 

Clara  F. shared her commented (annoted) version of the code that was uploaded in the 2023-01-31 folder for session 02 on the Gitlab repository for this group: https://git.doit.wisc.edu/bcrf/pythonclub/intermediate_advanced/studygroup

**Session 02** was based on Chapter 6 of the book with regular expressions as subject. 

Next time, **session 03** will be on [Chapter 7](https://github.com/amberbiology/py4lifesci/blob/master/PFTLS_Chapter_07.py) which more closely looks into *OOP*, Object Oriented Programming. 

***Who would like to volunteer to present this for session 03?*** Please send me an email!

## Beginners - February 02, 2023

For the next session we can run a Q/A session to answer pending questions from the previous sessions that covered Chapters 2 and 3. We can then continue on materials from Chapter 4 which introduces the Bayes methods for data analysis.

The "before" Jupyter notebook  based on the code content of [Chapter 4](https://github.com/amberbiology/py4lifesci/blob/master/PFTLS_Chapter_04.py) is now uploaded on Gitlab in session 4 directory [2023-02-02-Beginners-session-04](https://git.doit.wisc.edu/bcrf/pythonclub/beginners/studygroup/-/tree/main/2023-Spring/2023-02-02-Beginners-session-04)

---

### *Note*: Markdown

Yes, this email was first written in Markdown  
([Do yourself a favor: learn Markdown – Master it in 10 minutes!](https://bcrf.biochem.wisc.edu/2021/06/03/do-yourself-a-favor-learn-markdown-master-it-in-10-minutes/))


