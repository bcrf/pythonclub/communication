## Python Club - new material

Greetings all Club  Pythonists,

I am taking the opportunity of this reminder to address some questions.

>(Reminder: all content-worthy [communication emails are archived](https://git.doit.wisc.edu/bcrf/pythonclub/communication).

## Beginners

Beginners group meets today (Feb 16, 2023) from 4 to 5pm in room 201, Biochem Labs.

I am planning a short discussion to evaluate the potential of switching to a different book support, at least temporarily, to use this book:

>"[***Learn Python the right way***](https://www.dbooks.org/learn-python-the-right-way-5670579292/) - ***How to think like a computer scientist***" 2021 edition (GNU FDL license, free PDF download, 457 pages.)

I was surprised to see that the topic about dictionary (discussed in Februray 9 session) is arriving only at Chapter 20 in this book! What are we missing?!!

Therefore I am wondering if the current book is going too fast, at least for Beginners.

So here are some questions to ponder:

- Do you like the current book?
- Are explanations and examples clear to you?
- Would you like slower, more detailed curriculum (from the new book?)
- Should we perhaps keep both books?
- Is the club currently meeting your potential hopes and expectations?
- Is there anything that you would like different? What would that be?
- Should a different "format" be explored? *e.g.* follow an online video course (free for UW) and discuss and explore exercises in club sessions?

We can discuss this during the club session, and/or I could create an anonymous Google Doc or Qualtrics Q/A questionnaire for collecting ideas.

## Intermediate / Advanced

In the last session we explored Chapter 7 and its attempt to cover "Object Oriented Programming" (*OOP*.)

It seems that I am not the only one finding that the explanations about *OOP* as well as the examples are not very clear in the current book.

In the new book ""*Learn Python the right way*" menbtioned above, the subject of *OOP* is presented only in Chapter 15. I found that their explanation of `class` and `methods` was much clearer, and in addition they provide the vocabulary to describe what is happening with *e.g.* `__init__`: "initializer method","constructor", "instantiation", etc.

I suggest reading this chapter if you would like to clarify or refresh your understanding of *OOP* with more *OOP* discussed in chapters 16 and 22.

The "pondering questions" listed in the previous paragraph are also certainly relevant to this section...

Hopefully the Python Club has some good days ahead...

Jean-Yves



