## Python Club(s) news

Greeting Pythonists,

The early enthusiasm of the Python Club creation and registration that culminated with 43 registered members has provided the framework for 2 inclusive but separate session for Beginners and another for Intermediate/Advanced levels.

I have encouraged the use of one (free to UW) book to follow in order to provide structure and a way to have some "continuation" from session to session, especially if missing some of the sessions.

* Book: *Python for the Life Sciences – A Gentle Introduction*. [Download instructions](https://bcrf.biochem.wisc.edu/2022/11/15/python-for-the-life-sciences-a-gentle-introduction/) and its [python code](https://github.com/amberbiology/py4lifesci).

It is too early to forecast the future of the Club, and I am happy that for both sessions there are "core" members that attend regularly. Attendance numbers are listed in the tables below.

* **Is this book interesting to you?**
* *Is the text and the code understandable?*
* *Is the "Life Science" part helping?*
* *Are the premade Jupyter notebooks useful?*

Today in the Advanced group I mentioned another called ***Create Graphical User Interfaces with Python*** (Free download) that seemed to be of strong interest. I'll provide info about the book below.

* *Should we integrate this book in the sessions?*
* *If yes, should it replace or supplement the other book?*
* *Now? or later when we know more Python?*


***What do you think?***

***Is the club useful?***

Feel free to let me know your thoughts on how to keep the club as a useful entity for the club members.

JYS

P.S. Yes, this is written in *Markdown* first, including the tables below. We learn more about this in the R Club, but if you are curious/interested in text structuring we can too. This is indeed what is used to annotate Jupyter notebooks...write text in GitLab and GitHub...

##  GUI with Python

Here is the link to obtain the book [***Create Graphical User Interfaces with Python***](https://www.dbooks.org/create-graphical-user-interfaces-with-python-1912047918/)

### Description

Add buttons, boxes, pictures and colours and more to your Python programs using the guizero library, which is quick, accessible, and understandable for all.

This 156-page book is suitable for everyone, from beginners to experienced Python programmers who want to explore graphical user interfaces (GUIs).

There are ten fun projects for you to create, including a painting program, an emoji match game, and a stop-motion animation creator.

- Create games and fun Python programs;
- Learn how to create your own graphical user interfaces;
- Use windows, text boxes, buttons, images, and more;
- Learn about event-based programming;
- Explore good (and bad) user interface design.

This open book is licensed under a Creative Commons License (CC BY-NC-SA). You can download Create Graphical User Interfaces with Python ebook for free in PDF format (11.9 MB).

### Table of Contents

|  Chapters | Titles  |
| --------- | -----------|
|Chapter 1|Introduction to GUIs|
|Chapter 2|Wanted Poster|
|Chapter 3|Spy Name Chooser|
|Chapter 4|Meme Generator|
|Chapter 5|World's Worst GUI|
|Chapter 6|Tic-tac-toe|
|Chapter 7|Destroy the Dots|
|Chapter 8|Flood It|
|Chapter 9|Emoji Match|
|Chapter 10|Paint|
|Chapter 11|Stop-frame Animation|
|Appendix A|Setting up|
|Appendix B|Get started with Python|
|Appendix C|Widgets in guizero|

|  Details |  |
| --------- | -----------|
|Title|Create Graphical User Interfaces with Python
|Subject|Computer Science
|Publisher|Raspberry Pi Press
|Published|2020
|Pages|156
|Edition|1
|Language|English
|ISBN13 Digital|9781912047918
|ISBN10 Digital|1912047918
|PDF Size|11.9 MB
|License|CC BY-NC-SA


## Table of attendance in January/February

Beginners group meets every week for about 1h.

|  Beginners | attendance  |
|---|---|
| Jan 12  | 17  |
|  Jan 19 |  4 |
| Jan 26  |  7 |
|  Feb 2 |  5 |
|  Feb 9 | 4 |

Intermediate/Advance group meets every other week for about 2hours.

|  Interm/Adv |  attendance |
|---|---|
| Jan 17  | 7  |
| Jan 31  | 5  |
| Feb 14  | 6 |


