## Python Club Sessions

### Intermediate/Advanced - 28 February 2023

This is today...

We'll have fun exploring how to create a GUI interface for Python programs with [***Create Graphical User Interfaces with Python***](https://www.dbooks.org/create-graphical-user-interfaces-with-python-1912047918/). 

However, I noted that one important option is missing from the `guizero`: there is no provision to **open/close a file**. For this we'll add options from `tkinter` (package name: `tk`.)

### Beginners - 02 March 2023

We'll continue the flip format with [***Python Essential Training***](https://www.linkedin.com/learning/python-essential-training-18764650/getting-started-with-python), ([Jupyter notebooks](https://github.com/LinkedInLearning/python-essential-training-4314028).) We'll continue with:

- 3. Basic Data Types - total video time: 26 min 16.
- 4. Only the first item: "Lists" (5 min 21)



