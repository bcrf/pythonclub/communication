# Meeting #0 Summary

Thank you to the 18 participants that filled the Pre-Meeting survey. You can see the results in file ["pre-survey\_0.pdf"](https://uwmadison.box.com/s/r7kf7umfs1cb6o23q8cnxte8ile6wopo) in my BOX.

Thank you to those who attended the very first meeting to create the foundations for the Python Club. I realized later that I forgot to circulate the sign-in board, but we were a baker's dozen today.

### Here are the key points to remember:

0. We'll follow a code of conduct inspired by the Carpentries version (See below.)
1. There will be 2 groups: Beginner and Intermediate/Advanced. Both groups of interest are similar in size.
2. Groups will meet on separate date with their own agendas and their own frequency (see below).
3. At first both groups will follow the same book:
	- ["**Python for the Life Science**"](https://bcrf.biochem.wisc.edu/2022/11/15/python-for-the-life-sciences-a-gentle-introduction/) which can be downloaded from UW-Library as PDF (no cost to you, may need NetID.)
	- Club members may also work on the data from their projects (mostly in the Intermediate/Advanced club at first.) 
4. The Club will be "Semester long" to avoid "open-ended fatigue."
5. One or more UW-sponsored [GitLab repositories](git.doit.wisc.edu/) will contain studied materials.
6. When groups start meeting, we may establish a "*Slack*" or a "*Discord*" communication channel to facilitate exchange between group members.


## Schedule

**Beginner group**:   will meet every **Thursdays at 4pm** for **1h**, with an addional 1/2 hour.

**Intermediate/Advanced group**: will meet **Tuesdays 4pm-6pm** ***every other week***

## Set-up

Most users seem to prefer bringing their own computer, but the 6 classroom Mac minis are available as well.

It seems that many are already somewhat familiar with Anaconda versions, which has other pre-installed software that we'll use (*e.g.* Jupyter Lab notebook.)

**Thank you to Megan** who has offered to help with a set-up that will help everyone to use the same Python environment and also facilitate club sessions.


## Code of Conduct

In order to foster a positive and professional learning environment we encourage the following kinds of behaviours in all platforms and events:

- Use welcoming and inclusive language
- Be respectful of different viewpoints and experiences
- Gracefully accept constructive criticism
- Focus on what is best for the community
- Show courtesy and respect towards other community members

(Excerpt from [Carpentries Code of Conduct](https://docs.carpentries.org/topic_folders/policies/code-of-conduct.html#code-of-conduct-summary-view).)

## Happy Holidays

Enjoy the holidays and come with your refreshed enthusiasm in January,

Sincerely,  
Jean-Yves

---

P.S. For the curious... the simplest 4 lines of code that I mentioned that no longer work (if `pandas` => 1.5) were:

```python
import plotly.express as px
df = px.data.iris()
fig = px.scatter_matrix(df)
fig.show()
```

([Binder](https://hub.ovh2.mybinder.org/user/plotly-plotly.py-vlc4npux/notebooks/doc/python/splom.md))

P.P.S. This message was first written in "Markdown".