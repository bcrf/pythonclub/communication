# Python Club: getting started

Greetings all,

For now, I am sending email to all registrants, but in the future we may create separate lists for the 2 groups.  

If you registered today, I will have already sent you the Summary #0 and #0.1 about decisions made during the first, foundational meeting.

Let's get ready for the first Beginners Group meeting next week (but all are invited!)

- **Location**:  Biochemistry Laboratories Building, Room 201  
- **Time**: Thursday, January 12, 4pm (decided at foundational meeting.)

## Materials

We will follow the book "Python for the Life Sciences - A Gentle Introduction to Python for Life Scientists" by authors Alexander Lancaster and Gordon Webster. The book is free through the UW-Library system. Here are the links that may work "as is" if you are already logged-in with your *NetID*:

- Web site: https://link.springer.com/chapter/10.1007/978-1-4842-4523-1_1

- Full PDF: https://link.springer.com/content/pdf/10.1007/978-1-4842-4523-1.pdf

(I will also have one printed copy in the classroom for reference.)

## Teams channel / Repository

You will receive soon an invitation to the Python Club ***Teams*** channel, which is the only "official" UW-supported method (*i.e.* not Slack or Discord. You can download ***Teams*** from `software.wisc.edu` with your *NetID*.)

I am working on having a UW "Gitlab" repository set-up to hold the book code but also some code from the club sessions. More on this later.

## Computer

It seems that most will bring their laptop. However, if you need the classroom has 6 modern (M1 chip) Mac mini that can be used.


## Preparation /Set-up

We'll spend some time to alleviate issues of installation, versions etc. The current Python to learn is `Python 3` as `Python 2` has been officially retired.

 We'll discuss about which Python to use (*e.g.* Python.org or Anaconda) and the various advantages and problems.
 
 **It would be helpful** if you could read Chapter 1 "*Getting Started with Python*" (pages 1-12.)

 
## Code of conduct
 
 The club is open to all.   
 In order to foster a positive and professional learning environment we encourage the following kinds of behaviors in all platforms and events:

- Use welcoming and inclusive language
- Be respectful of different viewpoints and experiences
- Gracefully accept constructive criticism
- Focus on what is best for the community
- Show courtesy and respect towards other community members

(Excerpt from [Carpentries Code of Conduct](https://docs.carpentries.org/topic_folders/policies/code-of-conduct.html#code-of-conduct-summary-view).)


