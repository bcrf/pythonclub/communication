Greetings Python enthusiasts,

(For those of you that are not on the "everyone" email list of Biochem you might have not seen my note on ChatGPT - see below.)

In this context, for the Beginners session last week, I asked the ChatGPT AI to write a simple code for "biology students" to illustrate the use of a Python "Dictionary" structure to help compute the composition of a protein, with insulin as an example.
While the code works correctly, the example output does not match the insulin sequence.

I added a Jupyter Notebook in that session repository: [2023-Spring/2023-02-09-Beginners-session-05](https://git.doit.wisc.edu/bcrf/pythonclub/beginners/studygroup/-/tree/main/2023-Spring/2023-02-09-Beginners-session-05) that reviews the Python code, and verifies its output with an independent bash​ script.

While ChatGPT can provide amazing help, verifications may be in order to ascertain the validity of the proposed texts, code, facts that is provided.

We are witnessing one more change in the World that will have an impact on our lives....

JYS

-----------------------------------------------------------------------------------
<center>
**Event: Fri Feb 24, 2023 2-3pm**

<p style="color:red;">A Campus Conversation on ChatGPT</p>   
<p style="color:red;">in Teaching & Learning</p>

*What challenges and opportunities does ChatGPT present to instructors?*   
*How can we harness it instead of fearing it?*    
*How might it change teaching and learning in higher education?*   

**OPEN TO ALL** (Members, please invite your colleagues!)
Registration: [bit.ly/Register-ChatGPTinTL](https://bit.ly/Register-ChatGPTinTL)
</center>
